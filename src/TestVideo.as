package
{
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.StageVideoAvailabilityEvent;
	import flash.events.StageVideoEvent;
	import flash.geom.Rectangle;
	import flash.media.StageVideo;
	import flash.media.StageVideoAvailability;
	import flash.media.Video;
	import flash.net.NetConnection;
	import flash.net.NetStream;
	import flash.utils.setInterval;
	
	public class TestVideo extends Sprite
	{
		
		private var stageVideoAvail:Boolean;
		private var sv:StageVideo;
		private var ns:NetStream;

		private var stageWidth:int;
		private var stageHeight:int;
		
		private var pauseTimeout:int;
		private var isPaused:Boolean;
		
		private var videoName:String = 'assets/tir02_Sanfran iPad 1024x768.m4v';
		
		public function TestVideo()
		{
			super();
			this.addEventListener(Event.ADDED_TO_STAGE, onAddedToStage);
		}
		
		protected function onAddedToStage(event:Event):void
		{
			this.removeEventListener(Event.ADDED_TO_STAGE, onAddedToStage);
			
			stageWidth = stage.fullScreenWidth;
			stageHeight = stage.fullScreenHeight;
			
			if (stageHeight > stageWidth) {
				stageWidth = stageHeight;
				stageHeight = stage.fullScreenWidth;
				
			}
			
			
			trace (stageWidth +" :: "+stageHeight);
			
			stage.addEventListener(StageVideoAvailabilityEvent.STAGE_VIDEO_AVAILABILITY, onAvail);
				
		}
		
		private function onAvail(e:StageVideoAvailabilityEvent):void
		{
			stageVideoAvail = (e.availability == StageVideoAvailability.AVAILABLE);
			initVideo();
		}
		
		private function initVideo():void
		{
			var nc:NetConnection = new NetConnection();
			nc.connect(null);
			ns = new NetStream(nc);
			ns.client = this;
			
			if(stageVideoAvail)
			{
				sv = stage.stageVideos[0];
				sv.addEventListener(StageVideoEvent.RENDER_STATE, onRender);
				sv.attachNetStream(ns);
				trace('available');
			}
			else
			{
				var vid:Video = new Video(stageWidth, stageHeight);
				addChild(vid);
				vid.attachNetStream(ns);
				trace('not');
			}
			
			ns.play(videoName);
			
			pauseTimeout = setInterval(togglePause, 3000);
			
		}
		
		private function togglePause():void
		{
			trace (ns.time);
			
			if (isPaused) {
				ns.resume();
			} else {
				ns.pause();
			}
			
			isPaused = ! isPaused;
		}
		
		private function onRender(e:StageVideoEvent):void
		{
			sv.viewPort = new Rectangle(0, 0, stageWidth, stageHeight);
		}
		
		public function onMetaData(e:Object):void
		{
			
		}
		
		public function onXMPData(e:Object):void
		{
			
		}
		public function onPlayStatus(e:Object):void
		{
			//trace ("onPlayStatus" +e.code);
			if (e.code == 'NetStream.Play.Complete') {
				ns.seek(0);
				ns.resume();
			}
			
		}

		
	}
}