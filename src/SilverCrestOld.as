package
{
	import flash.display.Sprite;
	import flash.display.StageAlign;
	import flash.display.StageOrientation;
	import flash.display.StageScaleMode;
	import flash.events.Event;
	import flash.events.StageOrientationEvent;
	import flash.utils.setInterval;
	
	import net.hires.debug.Stats;
	
	[SWF(frameRate="60")]
	
	
	public class SilverCrestOld extends Sprite
	{
		
		private var testVideo:TestVideo;
		private var animationPoint:AnimationPoint;
		
		private var has3d:Boolean;
		
		public function SilverCrestOld()
		{
			super();
			
			// prend en charge autoOrients
			stage.align = StageAlign.TOP_LEFT;
			stage.scaleMode = StageScaleMode.NO_SCALE;
			
			
			applicationCompleteHandler();
			
			stage.setOrientation(StageOrientation.ROTATED_LEFT);
			
			addStageVideo();
			
			addFrameRater();
			
		}
		
		private function addStageVideo():void
		{
			
			testVideo = new TestVideo();
			this.addChild(testVideo);
			
			
			animationPoint = new AnimationPoint();
			this.addChild(animationPoint);
			
			for (var i:int = 0; i < 20; i++) 
			{
				var tmpPoint:AnimationPoint = new AnimationPoint();
				this.addChild(tmpPoint);
			}
			
			
			
			
		}
	
		private function addFrameRater():void
		{
			// TODO Auto Generated method stub
			var stats:Stats = new Stats();
			this.addChild(stats);
		}		
		
		private function applicationCompleteHandler():void {
			stage.addEventListener(StageOrientationEvent.ORIENTATION_CHANGING, stageOrientationChangingHandler);
			
		}
		
		private function stageOrientationChangingHandler(event:StageOrientationEvent):void {
			trace ("event.afterOrientation: "+event.afterOrientation);
			//setupSizes();
			event.preventDefault();
			/*
			if (event.afterOrientation == StageOrientation.DEFAULT || event.afterOrientation ==  StageOrientation.UPSIDE_DOWN)
			{
			event.preventDefault();
			}
			*/
			
			
		}
		
	}
}