package
{
	import com.greensock.TweenLite;
	
	import flash.display.Sprite;
	import flash.events.Event;
	
	public class AnimationPoint extends Sprite
	{
		private var basePoint:BasePoint;
		
		private var stageWidth:int;
		private var stageHeight:int;
		
		public function AnimationPoint()
		{
			super();
			this.addEventListener(Event.ADDED_TO_STAGE, onAddedToStage);
		}
		
		protected function onAddedToStage(event:Event):void
		{
			this.removeEventListener(Event.ADDED_TO_STAGE, onAddedToStage);
			
			stageWidth = stage.stageWidth;
			stageHeight = stage.stageHeight;
			
			stageWidth = stage.fullScreenWidth;
			stageHeight = stage.fullScreenHeight;
			
			if (stageHeight > stageWidth) {
				stageWidth = stageHeight;
				stageHeight = stage.fullScreenWidth;
				
			}
			
			basePoint = new BasePoint();
			this.addChild(basePoint);
			
			startRandomMove();
			
		}
		
		private function startRandomMove():void
		{
			var xTo:int = stageWidth * Math.random();
			var yTo:int = stageHeight * Math.random();
			var scaleTo:Number = Math.random()*1 + 1;
			
			TweenLite.to(basePoint, Math.random()*1 +1, {x: xTo, y: yTo, scaleX: scaleTo, scaleY: scaleTo, onComplete: startRandomMove, delay: Math.random()*3});
		
		}
	}
}