package fr.silvercrest.model.vo
{
	public class TextVO
	{
		public var text:String;
		public var startAt:Number;
		public var duration:Number;
		
		public function TextVO()
		{
		}
	}
}