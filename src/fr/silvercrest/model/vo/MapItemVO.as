package fr.silvercrest.model.vo
{
	public class MapItemVO
	{
		public var caption:String;
		public var type:int;
		public var points:Array;
		
		public function MapItemVO()
		{
		}
	}
}