package fr.silvercrest.model.vo
{
	public class PointVO
	{
		
		public var targetX:int;
		public var targetY:int;
		public var legendX:int;
		public var legendY:int;
		public var pageId:int;
		public var caption:String;
		public var body:String;
		public var displayAt:Number;
		
		public function PointVO()
		{
		}
	}
}