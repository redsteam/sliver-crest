package fr.silvercrest.model.events
{
	import flash.events.Event;
	
	public class NavigationEvent extends Event
	{
		public var pageId:int;
		
		public static const CHANGE_PAGE:String = "NavigationEvent.CHANGE_PAGE";
		public static const CHANGE_INNER_PAGE:String = "NavigationEvent.CHANGE_INNER_PAGE";

		public static const GO_HOME:String = "NavigationEvent.GO_HOME";
		public static const GO_BACK:String = "NavigationEvent.GO_BACK";
		public static const GO_NEXT:String = "NavigationEvent.GO_NEXT";
	
		public static const SHOW_NEXT:String = "NavigationEvent.SHOW_NEXT";
		public static const HIDE_NEXT:String = "NavigationEvent.HIDE_NEXT";
		
		
		public function NavigationEvent(type:String, bubbles:Boolean=false, cancelable:Boolean=false, pPageId:int=0)
		{
			super(type, bubbles, cancelable);
			pageId = pPageId;
			
		}
	}
}