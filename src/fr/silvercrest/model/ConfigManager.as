package fr.silvercrest.model {
	
	/**
	 * @author Red Steam Multimedia - www.redsteammultimedia.com - 2010
	 */
	 
	
	public class ConfigManager {
		
		
		static private var _instance:ConfigManager;
		
		
		public var sequence1:Array;
		public var sequence2:Array;
		public var mapItems:Array;
		public var motorsPoint:Array;
		public var mainPoints:Array;
		
		public var autoPlay:Boolean;
		
		
		//-----------------------------------------------------------
		
		public function ConfigManager() {
		}
		//-----------------------------------------------------------
		
		static public function getInstance():ConfigManager {
			if (_instance==null) {
			_instance = new ConfigManager();
			}
			return _instance;
		}

		
	}
}
