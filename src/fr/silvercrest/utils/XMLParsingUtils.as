package fr.silvercrest.utils
{
	import fr.silvercrest.model.vo.MapItemVO;
	import fr.silvercrest.model.vo.PointVO;
	import fr.silvercrest.model.vo.TextVO;
	import fr.silvercrest.model.vo.VideoVO;
	
	public class XMLParsingUtils
	{
		public function XMLParsingUtils()
		{
		}

		
		
	
		//-------------------------------------------------------------
		public static function parseConfig(itemList:XML):Array {
			var tmpArray:Array = new Array();
			
			for each (var m:XML  in itemList..config) {
					tmpArray[m.@id] = m.toString();	
				}
				
			return tmpArray;
		}
		//-------------------------------------------------------------
		public static function parseCaption(itemList:XML):Array {
			var tmpArray:Array = new Array();
			for each (var m:XML  in itemList..caption) {
					tmpArray[m.@id] = m;	
				}	
			return tmpArray;
		}
		
		
		//-------------------------------------------------------------
		public static function parsePoints(itemList:XML):Array {
			var tmpArray:Array = new Array();
			
			var itemVO:PointVO;
			for each (var m:XML  in itemList..point) {
				
				itemVO = new PointVO();
				
				itemVO.displayAt = Number(m.@displayAt);
				itemVO.pageId = int(m.@pageId);

				itemVO.targetX = m.targetPosition.@x;
				itemVO.targetY = m.targetPosition.@y;
				
				itemVO.legendX = m.legend.@x;
				itemVO.legendY = m.legend.@y;
				
				itemVO.caption = m.legend.caption;
				itemVO.body = m.legend.body;
				
				tmpArray.push(itemVO);
				
			}
			return tmpArray;
		}
		
		//-------------------------------------------------------------
		public static function parseVideos(itemList:XML):Array {
			var tmpArray:Array = new Array();
			
			var itemVO:VideoVO;
			for each (var m:XML  in itemList..video) {
				
				itemVO = new VideoVO();
				
				itemVO.file = m.@file;
				itemVO.texts = parseTexts(m.texts[0]);
				tmpArray.push(itemVO);
				
			}
			return tmpArray;
		}
		//-------------------------------------------------------------
		public static function parseTexts(itemList:XML):Array {
			var tmpArray:Array = new Array();
			trace ("itemList: "+itemList);
			if (!itemList) return tmpArray;
			var itemVO:TextVO;
			for each (var m:XML  in itemList..text) {
				
				itemVO = new TextVO();
				
				itemVO.startAt = Number(m.@startAt);
				itemVO.duration = Number(m.@duration);
				itemVO.text = m;
				tmpArray.push(itemVO);
				
			}
			return tmpArray;
		}
		//-------------------------------------------------------------
		public static function parseMapItems(itemList:XML):Array {
			var tmpArray:Array = new Array();
			
			var itemVO:MapItemVO;
			for each (var m:XML  in itemList..mapItem) {
				
				itemVO = new MapItemVO();
				
				itemVO.type = m.@type;
				itemVO.caption = m.caption;
				itemVO.points = parsePoints(m.points[0]);
				tmpArray.push(itemVO);
				
			}
			return tmpArray;
		}
		
		
		//-------------------------------------------------------------
		
	}
}