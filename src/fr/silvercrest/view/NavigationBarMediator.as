package fr.silvercrest.view {
	
	/**
	 * @author Red Steam Multimedia - www.redsteammultimedia.com - 2013
	 */
	
	import flash.events.Event;
	
	import fr.silvercrest.SilverCrest;
	import fr.silvercrest.constants.ApplicationConstants;
	import fr.silvercrest.constants.NotificationConstants;
	import fr.silvercrest.model.ConfigurationProxy;
	import fr.silvercrest.model.MainProxy;
	import fr.silvercrest.model.events.NavigationEvent;
	import fr.silvercrest.view.content.ContentDisplayer;
	import fr.silvercrest.view.content.navigation.NavigationBar;
	
	import org.puremvc.as3.interfaces.IMediator;
	import org.puremvc.as3.interfaces.INotification;
	import org.puremvc.as3.patterns.mediator.Mediator;
	
	public class NavigationBarMediator extends Mediator implements IMediator
	{
		public static const NAME:String = "NavigationBarMediator";
		
		private var mainProxy:MainProxy;
		
		//-------------------------------------------------------
		
		protected function get navigationBar():NavigationBar
		{
			return viewComponent as NavigationBar
		}
		
		public function NavigationBarMediator(viewComponent:Object=null)
		{
			super(NAME, viewComponent);
			
			navigationBar.addEventListener(NavigationEvent.GO_HOME,onGoHome);
			navigationBar.addEventListener(NavigationEvent.GO_BACK,onGoBack);
			navigationBar.addEventListener(NavigationEvent.GO_NEXT,onGoNext);
			
		}
		
	
		
		protected function onGoNext(event:Event):void
		{
			sendNotification(NotificationConstants.GO_NEXT);
			
		}
		
		protected function onGoBack(event:Event):void
		{
			sendNotification(NotificationConstants.GO_BACK);
			
		}
		
		protected function onGoHome(event:Event):void
		{
			sendNotification(NotificationConstants.CONTENT_CHANGE, ApplicationConstants.CONTENT_HOME);
		}		
		
		
		
		override public function listNotificationInterests():Array
		{
			return [ 	NotificationConstants.LOAD_CONFIGURATION_SUCCESS,
				NotificationConstants.CONTENT_CHANGE,
				NotificationConstants.SHOW_NEXT,
				NotificationConstants.HIDE_NEXT,
				
				
				
			];
			
		}
		
		override public function handleNotification(notification:INotification):void
		{
			
			switch ( notification.getName() ) 
			{
				
				case NotificationConstants.LOAD_CONFIGURATION_SUCCESS:
					//navigationBar.show();
					break;					
				
				case NotificationConstants.CONTENT_CHANGE:
					navigationBar.setState(notification.getBody() as String);
				break;
				
				case NotificationConstants.SHOW_NEXT:
					navigationBar.showNext(true);
					navigationBar.show();
				break;
				
				case NotificationConstants.HIDE_NEXT:
					navigationBar.show();
					navigationBar.showNext(false);
				break;
				
			
				
				
				
			}
			
		}
		
		
		
		
	}
}
