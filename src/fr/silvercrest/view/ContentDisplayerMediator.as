package fr.silvercrest.view {
	
	/**
	 * @author Red Steam Multimedia - www.redsteammultimedia.com - 2013
	 */
	
	import flash.events.Event;
	
	import fr.silvercrest.SilverCrest;
	import fr.silvercrest.constants.ApplicationConstants;
	import fr.silvercrest.constants.NotificationConstants;
	import fr.silvercrest.model.ConfigurationProxy;
	import fr.silvercrest.model.MainProxy;
	import fr.silvercrest.model.events.NavigationEvent;
	import fr.silvercrest.view.content.ContentDisplayer;
	import fr.silvercrest.view.content.videoSequence.VideoSequence;
	
	import org.puremvc.as3.interfaces.IMediator;
	import org.puremvc.as3.interfaces.INotification;
	import org.puremvc.as3.patterns.mediator.Mediator;
	
	public class ContentDisplayerMediator extends Mediator implements IMediator
	{
		public static const NAME:String = "ContentDisplayerMediator";
		
		private var mainProxy:MainProxy;
		
		//-------------------------------------------------------
		
		protected function get contentDisplayer():ContentDisplayer
		{
			return viewComponent as ContentDisplayer
		}
		
		public function ContentDisplayerMediator(viewComponent:Object=null)
		{
			super(NAME, viewComponent);
			
			contentDisplayer.addEventListener(NavigationEvent.CHANGE_PAGE, onChangePage );
			contentDisplayer.addEventListener(NavigationEvent.GO_HOME,onGoBackHome);
			contentDisplayer.addEventListener(NavigationEvent.SHOW_NEXT,onShowNext);
			contentDisplayer.addEventListener(NavigationEvent.HIDE_NEXT,onHideNext);
			
		}
		
	
		
		protected function onShowNext(event:Event):void
		{
			sendNotification(NotificationConstants.SHOW_NEXT);
			
		}
		
		protected function onHideNext(event:Event):void
		{
			sendNotification(NotificationConstants.HIDE_NEXT);
			
		}
		
		protected function onGoBackHome(event:NavigationEvent):void
		{
			sendNotification(NotificationConstants.CONTENT_CHANGE, ApplicationConstants.CONTENT_HOME);
			
		}
		
		protected function onChangePage(event:NavigationEvent):void
		{
		
			var contentTo:String;
			
			switch (event.pageId) {
				case 0:
					// moscow
					contentTo = ApplicationConstants.CONTENT_MOSCOW;
				break;
				
				case 1:
					// plane
					contentTo = ApplicationConstants.CONTENT_PLANE;
				break;
				
				case 2:
					// nassau
					contentTo = ApplicationConstants.CONTENT_NASSAU;
				break;
				
			
			}
			sendNotification(NotificationConstants.CONTENT_CHANGE, contentTo);
			
		}		
		
		override public function onRegister( ):void {
			
			facade.registerMediator(new NavigationBarMediator(contentDisplayer.navigationBar));
		}
		
		override public function listNotificationInterests():Array
		{
			return [ 	NotificationConstants.LOAD_CONFIGURATION_SUCCESS,
			
				NotificationConstants.CONTENT_CHANGE,
				NotificationConstants.GO_BACK,
				NotificationConstants.GO_NEXT
				
			];
			
		}
		
		override public function handleNotification(notification:INotification):void
		{
			
			switch ( notification.getName() ) 
			{
				
				case NotificationConstants.LOAD_CONFIGURATION_SUCCESS:
					contentDisplayer.initContent();
					contentDisplayer.display(ApplicationConstants.CONTENT_HOME);
					break;					
				
				case NotificationConstants.CONTENT_CHANGE:
					contentDisplayer.display(notification.getBody() as String);
					break;					
			
				
				case NotificationConstants.GO_BACK:
					contentDisplayer.goBack();
				break;

				case NotificationConstants.GO_NEXT:
					contentDisplayer.goNext();
				break;
				
			}
			
		}
		
	
		
		
	}
}
import fr.silvercrest.view;

