package fr.silvercrest.view.content
{
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.utils.setTimeout;
	
	import fr.silvercrest.constants.ApplicationConstants;
	import fr.silvercrest.model.ConfigManager;
	import fr.silvercrest.model.events.NavigationEvent;
	import fr.silvercrest.view.content.home.HomePageDisplay;
	import fr.silvercrest.view.content.navigation.NavigationBar;
	import fr.silvercrest.view.content.plane.PlaneDisplay;
	import fr.silvercrest.view.content.videoSequence.VideoSequence;
	import fr.silvercrest.view.content.videoSequence.VideoSequencer;
	
	public class ContentDisplayer extends Sprite
	{
		
		
		public var homePageDisplay:HomePageDisplay;
		public var nassauDisplay:VideoSequencer;
		public var moscowDisplay:VideoSequencer;
		public var planeDisplay:PlaneDisplay;
		
		public var currentContent:AbstractContent;
		
		public var navigationBar:NavigationBar;
		
		public var itemsContainer:Sprite;
		
		public function ContentDisplayer()
		{
			super();
			initChildren();
			
		}
		
		private function initChildren():void {
			itemsContainer = new Sprite();
			this.addChild(itemsContainer);
			
			homePageDisplay = new HomePageDisplay();
			itemsContainer.addChild(homePageDisplay);
			
			nassauDisplay = new VideoSequencer();
			itemsContainer.addChild(nassauDisplay);
			
			moscowDisplay = new VideoSequencer();
			itemsContainer.addChild(moscowDisplay);
			
			planeDisplay = new PlaneDisplay();
			itemsContainer.addChild(planeDisplay);
			
			itemsContainer.addEventListener(VideoSequence.VIDEO_STARTED,onVideoStarted);
			itemsContainer.addEventListener(VideoSequence.VIDEO_ENDED,onVideoEnded);
			
			navigationBar = new NavigationBar();
			this.addChild(navigationBar);
			navigationBar.y = ApplicationConstants.INNER_CONTENT_HEIGHT;
		}
		
		protected function onVideoEnded(event:Event):void
		{
			event.stopPropagation();
			
			//dispatchEvent(new NavigationEvent(NavigationEvent.GO_HOME));
			
		}
		
		public function initContent():void
		{
			planeDisplay.initContent();
			nassauDisplay.sequences = ConfigManager.getInstance().sequence1;
			moscowDisplay.sequences = ConfigManager.getInstance().sequence2;
		}
		
		
		protected function onVideoStarted(event:Event):void
		{
			homePageDisplay.forceHide();
		}		
		
		private function dispatchShowNext():void {
		
			dispatchEvent(new NavigationEvent(NavigationEvent.SHOW_NEXT, true));
		}
		
		public function display(value:String):void {
			
			trace ("content change: "+value);
			
			var tmpContent:AbstractContent;
			
			switch (value) {
			
				case ApplicationConstants.CONTENT_HOME:
					
					// make sure we can go home!
					if (currentContent == planeDisplay) {
						if (planeDisplay.currentSequenceCounter>0) {
							
							planeDisplay.displayContentNum(0);
							setTimeout(dispatchShowNext, 10);
							return;
							
						}
					}
					
					tmpContent = homePageDisplay as AbstractContent;
//					tmpContent = planeDisplay as AbstractContent;
				break;

				case ApplicationConstants.CONTENT_NASSAU:
					tmpContent = nassauDisplay as AbstractContent;
				break;
				
				case ApplicationConstants.CONTENT_MOSCOW:
					tmpContent = moscowDisplay as AbstractContent;
				break;
				
				case ApplicationConstants.CONTENT_PLANE:
					tmpContent = planeDisplay as AbstractContent;
					break;

				
			}
			
			if (tmpContent && tmpContent!= currentContent) {
				
				if (currentContent) currentContent.hide();
				tmpContent.show();
				
				currentContent = tmpContent;
				
			}
		
		}
		
		
		public function goBack():void
		{
			if (currentContent) {
				currentContent.goBack();
			}
			
		}
		
		public function goNext():void
		{
			if (currentContent) {
				currentContent.goNext();
			}
			
		}
		
		
	}
}