package fr.silvercrest.view.content
{
	import flash.display.Sprite;
	
	public class AutoHideSprite extends Sprite
	{
		override public function set alpha(value:Number):void {
			super.alpha = value;
			this.visible = value>0;
		}
		
		public function AutoHideSprite()
		{
			super();
			
		}
	}
}