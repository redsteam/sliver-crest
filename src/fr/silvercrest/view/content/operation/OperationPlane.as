package fr.silvercrest.view.content.operation
{
	import flash.display.Sprite;
	
	import fr.silvercrest.constants.ApplicationConstants;
	
	public class OperationPlane extends Sprite
	{
		
		private var planeContainer:OperationPlaneUI;
		
		public static const X_MARGIN:int = 300;
		
		
		public function OperationPlane()
		{
			super();
			planeContainer = new OperationPlaneUI();
			this.addChild(planeContainer);
			planeContainer.stop();
			
		}
		override public function set x (value:Number):void {
			super.x = value;
			setupFrame();
			
		}
		
		override public function get width():Number {
			return 1440;
		}
		
		public function setupFrame():void {
			
			
			var minX:int = Math.abs(ApplicationConstants.APPLICATION_WIDTH - planeContainer.width-OperationPlane.X_MARGIN);
			var maxX:int = OperationPlane.X_MARGIN;
			
			var frameTo:int = Math.round((1-((this.x + minX ) / (minX + maxX)))* (planeContainer.totalFrames-1)+1);
			
			planeContainer.gotoAndStop(frameTo);
			
		}
		
	}
}