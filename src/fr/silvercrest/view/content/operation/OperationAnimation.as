package fr.silvercrest.view.content.operation
{
	import com.greensock.TweenLite;
	
	import flash.events.Event;
	import flash.events.TouchEvent;
	
	import fr.silvercrest.constants.ApplicationConstants;
	import fr.silvercrest.view.content.AbstractContent;
	
	public class OperationAnimation extends AbstractContent
	{
		
		private var bkg:OperationBkgUI;
		private var title:OperationTitleUI;
		private var planeContainer:OperationPlane;
		
		private var _isDragging:Boolean;
		private var _previousMouseX:Number;
		private var _lastSpeed:Number;
		private var alphaPad:alphaPadUI;
		
		
		public function OperationAnimation()
		{
			super();
			tweenSpeed = 0.5;
			this.addEventListener(Event.ADDED_TO_STAGE,onAddedToStage);
		}
		
		
		public function initContent():void
		{
			
		}
		
		protected function onAddedToStage(event:Event):void
		{
			this.removeEventListener(Event.ADDED_TO_STAGE,onAddedToStage);
			this.initChildren();
			setupTouchFunctions();
			
		}
		
		private function initChildren():void {
			bkg = new OperationBkgUI();
			this.addChild(bkg);
			
			planeContainer = new OperationPlane();
			this.addChild(planeContainer);
			
			
			title = new OperationTitleUI();
			this.addChild(title);
			
			
		
			alphaPad = new alphaPadUI();
			alphaPad.width = ApplicationConstants.APPLICATION_WIDTH;
			alphaPad.height = ApplicationConstants.INNER_CONTENT_HEIGHT;
			this.addChild(alphaPad);
			
		
			
		}
		
		
		private function setupTouchFunctions() : void {
			
			
			alphaPad.addEventListener(TouchEvent.TOUCH_BEGIN, onThumbDown); 
			stage.addEventListener(TouchEvent.TOUCH_MOVE, onTouchMove); 
			stage.addEventListener(TouchEvent.TOUCH_END, onThumbUp); 
			
			
		}
		
		private function onThumbDown(event:TouchEvent) : void {
			if (!_isActive) return;
			_previousMouseX = event.stageX;
			_lastSpeed  =0;
			_isDragging = true;
			TweenLite.killTweensOf(planeContainer);
			
		}
		
		private function onThumbUp(event:TouchEvent) : void {
			if (!_isActive) return;
			//	if (_isDragging)_justReleased = true;
			if (_isDragging) {
				_isDragging = false;
				
				replacePointsDisplayer();
			}	
		}
		
		private function replacePointsDisplayer():void
		{
			var xTo:int = planeContainer.x+_lastSpeed;
			
			if (xTo>OperationPlane.X_MARGIN) xTo = OperationPlane.X_MARGIN;
			if (xTo<(ApplicationConstants.APPLICATION_WIDTH - planeContainer.width-OperationPlane.X_MARGIN))xTo=ApplicationConstants.APPLICATION_WIDTH - planeContainer.width-OperationPlane.X_MARGIN;
			
			
			//trace ("_lastSpeed:" +_lastSpeed);
			
			displayContent(xTo, false, _lastSpeed*16);
			
		}
		
		private function onTouchMove(event:TouchEvent) : void {
			if (!_isActive) return;
			if (_isDragging) {
				dispatchValueChanged(event);
				//	setTouchZoneListeners(false);
			}
		}
		
		private function dispatchValueChanged(event:TouchEvent) : void {
			
			_lastSpeed = (event.stageX - _previousMouseX)*0.8;
			
			planeContainer.x += _lastSpeed;
			
			
			_previousMouseX = event.stageX;
			
		}
		
		private function displayContent(value:int, isDirect:Boolean = false, velocity:Number= 500):void {
			
			TweenLite.killTweensOf(planeContainer);
			
			if (isDirect) {
				TweenLite.to(planeContainer, 0, {x: value});
			} else {			
				TweenLite.to(planeContainer, 0.5, {throwProps:{x:{velocity:velocity, max:OperationPlane.X_MARGIN, min:ApplicationConstants.APPLICATION_WIDTH - planeContainer.width-OperationPlane.X_MARGIN}}});
			}
			
			
			
		}
	
		
		override protected function initSequence():void {
			displayContent(OperationPlane.X_MARGIN, true);
		}
		
	}
}