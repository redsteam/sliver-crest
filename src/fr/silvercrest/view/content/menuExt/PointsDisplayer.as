package fr.silvercrest.view.content.menuExt
{
	import flash.display.Sprite;
	import flash.utils.clearTimeout;
	import flash.utils.setTimeout;
	
	import fr.silvercrest.model.vo.PointVO;
	
	public class PointsDisplayer extends Sprite
	{
		
		private var _points:Array;
		private var _timeouts:Array = new Array();
		
		public function set points(value:Array):void {
			_points = value;
			initContent();
		}
		
		public function get points():Array{
			return _points;
		}
		
		
		public function PointsDisplayer()
		{
			super();
		}
		
		private function initContent():void {
		
		
			var lng:int = points.length;
			var pointDisplay:PointDisplay;
			
			for (var i:int = 0; i < lng; i++) 
			{
				pointDisplay = new PointDisplay();
				this.addChild(pointDisplay);
				pointDisplay.pointVO = points[i] as PointVO;
				
			}
			
		}
		
		
		public function displayContent():void {
		
			clearTimeouts();
			var lng:int = this.numChildren;
			var pointDisplay:PointDisplay;
			
			for (var i:int = 0; i < lng; i++) 
			{
				pointDisplay = this.getChildAt(i) as PointDisplay;
				pointDisplay.hide(true);
				_timeouts[i] = setTimeout(displayPoint, pointDisplay.pointVO.displayAt * 1000, pointDisplay);
			}
			
			
		}
		
		private function displayPoint(value:PointDisplay):void {
			value.show();
		
		}
		
		private function clearTimeouts():void {
			if (!_timeouts) return;
			var lng:int = _timeouts.length;
			for (var i:int = 0; i < lng; i++) 
			{
				clearTimeout(_timeouts[i]);
			}
			
		}
		
		public function setValue(value:Number, isDirect:Boolean = false):void {
			
			// display or hide points depending on their value
			var lng:int = this.numChildren;
			var pointDisplay:PointDisplay;
			
			for (var i:int = 0; i < lng; i++) 
			{
				pointDisplay = this.getChildAt(i) as PointDisplay;
				if (pointDisplay.pointVO.displayAt<=value){
					pointDisplay.show(isDirect);
				} else {
					pointDisplay.hide(isDirect);
				}
			}
			
		}
	}
}