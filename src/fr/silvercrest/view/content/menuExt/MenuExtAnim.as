package fr.silvercrest.view.content.menuExt
{
	import flash.events.Event;
	
	import fr.silvercrest.model.ConfigManager;
	import fr.silvercrest.view.content.AbstractContent;
	
	public class MenuExtAnim extends AbstractContent
	{
		private var bkg:MainExtBkgUI;
		private var pointsDisplayer:PointsDisplayer;
		public function MenuExtAnim()
		{
			super();
			
			
			tweenSpeed = 0.5;
			this.addEventListener(Event.ADDED_TO_STAGE,onAddedToStage);
			
		}
		
		protected function onAddedToStage(event:Event):void
		{
			this.removeEventListener(Event.ADDED_TO_STAGE,onAddedToStage);
			this.initChildren();
			setupTouchFunctions();
			
		}
		
		
		private function setupTouchFunctions() : void {
			
			
		}
		
		public function initContent():void
		{
			pointsDisplayer.points = ConfigManager.getInstance().mainPoints;
		}
		
		private function initChildren():void {
			bkg = new MainExtBkgUI();
			this.addChild(bkg);
		
			
			pointsDisplayer = new PointsDisplayer();
			this.addChild(pointsDisplayer);
			
			
			
		}
		
		override protected function initSequence():void {
			pointsDisplayer.displayContent();
		}
		
	}
}