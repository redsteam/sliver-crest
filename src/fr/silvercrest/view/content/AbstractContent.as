package fr.silvercrest.view.content
{
	import com.greensock.TweenLite;
	
	import flash.display.Sprite;
	
	public class AbstractContent extends Sprite
	{
		
		protected var _isActive:Boolean;
		
		public var tweenSpeed:Number = 0;
		
		override public function set alpha(value:Number):void {
			super.alpha = value;
			this.visible = value>0;
		}
		
		public function AbstractContent()
		{
			super();
			this.alpha = 0;
		}
		
		public function hide():void {
			display(false);
		}
		
		public function show():void {
			display(true);
		}
		
		protected function display(value:Boolean):void {
			
			
			TweenLite.to(this, tweenSpeed, {alpha:value?1:0});
			
			_isActive = value;
			
			if (value) {
				initSequence();
			} else {
				stopSequence();
			}
			
		}
		
		protected function initSequence():void {}
		
		protected function stopSequence():void {}

		public function goBack():void {}
		public function goNext():void {}
	}
}