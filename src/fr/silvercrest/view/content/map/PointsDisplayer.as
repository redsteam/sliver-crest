package fr.silvercrest.view.content.map
{
	import flash.display.Sprite;
	
	import fr.silvercrest.model.vo.PointVO;
	import fr.silvercrest.view.content.map.PointDisplay;
	
	public class PointsDisplayer extends Sprite
	{
		
		private var _points:Array;
		public var type:int;
		
		public function set points(value:Array):void {
			_points = value;
			initContent();
		}
		
		public function get points():Array{
			return _points;
		}
		
		
		
		public function PointsDisplayer()
		{
			super();
		}
		
		private function initContent():void {
			
			
			var lng:int = points.length;
			var pointDisplay:PointDisplay;
			
			for (var i:int = 0; i < lng; i++) 
			{
				pointDisplay = new PointDisplay();
				this.addChild(pointDisplay);
				pointDisplay.type = type;
				pointDisplay.pointVO = points[i] as PointVO;
				pointDisplay.show();
			}
			
		}
	}
}