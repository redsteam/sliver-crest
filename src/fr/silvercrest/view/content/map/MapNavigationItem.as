package fr.silvercrest.view.content.map
{
	import flash.display.Sprite;
	import flash.events.TouchEvent;
	
	import fr.silvercrest.model.events.NavigationEvent;
	
	public class MapNavigationItem extends Sprite
	{
		private var mapNavigationItemUI:MapNavigationItemUI;
		
		private var _active:Boolean;
		public var id:int;
		
		public function get active():Boolean {
			return _active;
		}

		public function set active(value:Boolean):void {
			_active = value;
			mapNavigationItemUI.gotoAndStop(active?2:1);
		}
		
		public function MapNavigationItem()
		{
			super();
			
			mapNavigationItemUI = new MapNavigationItemUI();
			this.addChild(mapNavigationItemUI);
			mapNavigationItemUI.stop();
			
			mapNavigationItemUI.addEventListener(TouchEvent.TOUCH_TAP, onBtnTouch);
		}
		
		protected function onBtnTouch(event:TouchEvent):void
		{
			
			dispatchEvent(new NavigationEvent(NavigationEvent.CHANGE_PAGE, true, false, this.id));
			
		}		
		
		
	}
}