package fr.silvercrest.view.content.map
{
	import flash.display.Sprite;
	
	public class MapNavigation extends Sprite
	{
		private var _mapItems:Array;
		
		public function set mapItems(value:Array):void {
			_mapItems = value;
			setupDisplay();
		}
			
		
		public function get mapItems():Array {
			return _mapItems;
		}
		
		public function MapNavigation()
		{
			super();
		}
		
		public function setActive(value:int):void {
		
			var lng:int = this.numChildren;
			var mapNavigationItem:MapNavigationItem;
			
			for (var i:int = 0; i < lng; i++) 
			{
				mapNavigationItem = this.getChildAt(i) as MapNavigationItem;
				mapNavigationItem.active = mapNavigationItem.id == value;
			}
			
		}
		
		private function setupDisplay():void
		{
			// create navigation
			var baseX:int = 0;
			var lng:int = mapItems.length;
			var mapNavigationItem:MapNavigationItem;
			for (var i:int = 0; i < lng; i++) 
			{	
				mapNavigationItem = new MapNavigationItem();
				this.addChild(mapNavigationItem);
				mapNavigationItem.id = i;
				mapNavigationItem.x = baseX;
				baseX += 45;
			}
			
			this.x = Math.round((stage.fullScreenWidth - this.width)/2);
			
			
		}
		
		
	}
}