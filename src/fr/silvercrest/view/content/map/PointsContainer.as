package fr.silvercrest.view.content.map
{
	import flash.display.Sprite;
	
	import fr.silvercrest.model.vo.MapItemVO;
	import fr.silvercrest.view.content.map.PointsDisplayer;
	
	public class PointsContainer extends Sprite
	{
		
		private var _mapItems:Array;
		
		
		private var _x:Number=0;
		
		override public function set x(value:Number):void {
			_x = value;
			setPosition();
		}
		
		override public function get x():Number {
			return _x;
		}
		
		public function set mapItems(value:Array):void {
			_mapItems = value;
			setupDisplay();
		}
		
		
		public function get mapItems():Array {
			return _mapItems;
		}
		
		public function PointsContainer()
		{
			super();
		}
		
		private function setupDisplay():void
		{
			
			var baseX:int = 0;
			var lng:int = mapItems.length;
			var pointsDisplayer:PointsDisplayer;
			for (var i:int = 0; i < lng; i++) 
			{	
				pointsDisplayer = new PointsDisplayer();
				this.addChild(pointsDisplayer);
				pointsDisplayer.type = (mapItems[i] as MapItemVO).type
				pointsDisplayer.points = (mapItems[i] as MapItemVO).points;
				pointsDisplayer.x = baseX;
				baseX = stage.fullScreenWidth;
			} 
		}
		
		private function setPosition():void {
			
			var lng:int = this.numChildren;
			var pointsDisplayer:PointsDisplayer;
			
			trace ("x: "+_x);
			var screenWidth:int = stage.fullScreenWidth;
			
			for (var i:int = 1; i < lng; i++) 
			{
				pointsDisplayer = this.getChildAt(i) as PointsDisplayer;
				
				var xTo:int = 0;
				
				if (i==1) {
					
					if (x < -screenWidth){
						xTo = 0;
					} else if (x>=-screenWidth && x<=screenWidth) {
					
						xTo =x + screenWidth;
					} else {
						xTo = screenWidth;
					}
					
					/*
					if (_x>(i-1)*stage.fullScreenWidth) {
						xTo = stage.fullScreenWidth
					}
					if (_x<=(i-1)*stage.fullScreenWidth && _x>= -(i+1)*stage.fullScreenWidth) {
						xTo = _x + (i-1)*stage.fullScreenWidth;
					}
				
					if (_x<-(i+1)*stage.fullScreenWidth) xTo = 0;
					
					*/
				} else if (i==2) {
					if (x < -2*screenWidth){
						xTo = 0;
					} else if (x>=-2*screenWidth && x<=2*screenWidth) {
						
						xTo =x + 2*screenWidth;
					} else {
						xTo = screenWidth;
					}
				
				}
				
				
				
				/*
				
				if (_x>-i*stage.fullScreenWidth) {
					xTo = stage.fullScreenWidth
				} else if (_x>=-(i+1)*stage.fullScreenWidth) {
					xTo = _x + i* stage.fullScreenWidth;
				} else {
					
					xTo = 0;
				}
				*/
			//	trace ("i: "+i+" xTo: "+xTo);
				
				
				pointsDisplayer.x = xTo;
				
				
  			}
			
		
		}
		
	}
}