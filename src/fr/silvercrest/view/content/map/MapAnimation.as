package fr.silvercrest.view.content.map
{
	import com.greensock.TweenLite;
	
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.TouchEvent;
	
	import fr.silvercrest.model.ConfigManager;
	import fr.silvercrest.model.events.NavigationEvent;
	import fr.silvercrest.model.vo.MapItemVO;
	import fr.silvercrest.view.content.AbstractContent;
	
	public class MapAnimation extends AbstractContent
	{
		private var bkg:MapBkgUI;
		private var _mapItems:Array;
		private var mapNavigation:MapNavigation;
		private var _isDragging:Boolean;
		private var _previousMouseX:Number;
		private var alphaPad:alphaPadUI;
		private var pointsContainer:PointsContainer;
		private var logoUI:MapLogoUI;
		
		
		public function set mapItems(value:Array):void {
			_mapItems = value;
			setupDisplay();
		}
		
	
		
		public function get mapItems():Array {
			return _mapItems;
		}
		public function MapAnimation()
		{
			super();
			tweenSpeed = 0.5;
			this.addEventListener(Event.ADDED_TO_STAGE,onAddedToStage);
		}
		
		protected function onAddedToStage(event:Event):void
		{
			this.removeEventListener(Event.ADDED_TO_STAGE,onAddedToStage);
			this.initChildren();
			setupTouchFunctions();
			
		}
		
		
		private function setupTouchFunctions() : void {
			
			
			alphaPad.addEventListener(TouchEvent.TOUCH_BEGIN, onThumbDown); 
			stage.addEventListener(TouchEvent.TOUCH_MOVE, onTouchMove); 
			stage.addEventListener(TouchEvent.TOUCH_END, onThumbUp); 
			
			
			
			
		}
		
	
		private function onThumbDown(event:TouchEvent) : void {
			if (!_isActive) return;
			_previousMouseX = event.stageX;
			_isDragging = true;
			TweenLite.killTweensOf(pointsContainer);
			
		}
		
		private function onThumbUp(event:TouchEvent) : void {
			if (!_isActive) return;
		//	if (_isDragging)_justReleased = true;
			if (_isDragging) {
				_isDragging = false;
			
				replacePointsDisplayer();
			}	
		}
		
		private function replacePointsDisplayer():void
		{
			var xTo:int = pointsContainer.x;
			
			if (xTo>0) xTo = 0;
			if (xTo<-(mapItems.length-1)*stage.fullScreenWidth)xTo=-(mapItems.length-1)*stage.fullScreenWidth
			
			var pageTo:int = Math.round(-xTo/stage.fullScreenWidth);
			
			displayContent(pageTo);
				
		}
		
		private function onTouchMove(event:TouchEvent) : void {
			if (!_isActive) return;
			if (_isDragging) {
				dispatchValueChanged(event);
			//	setTouchZoneListeners(false);
			}
		}
		
		private function dispatchValueChanged(event:TouchEvent) : void {
			
			
			pointsContainer.x += (event.stageX - _previousMouseX)*2.5;
			
			
			_previousMouseX = event.stageX;
			
		}
		
		
		private function initChildren():void {
			bkg = new MapBkgUI();
			this.addChild(bkg);
			
			pointsContainer = new PointsContainer();
			this.addChild(pointsContainer);
			
			logoUI = new MapLogoUI();
			this.addChild(logoUI);
			logoUI.x = 35;
			logoUI.y = 585;
			logoUI.stop();
			
			
			alphaPad = new alphaPadUI();
			alphaPad.width = stage.fullScreenWidth;
			alphaPad.height = stage.fullScreenHeight;
			this.addChild(alphaPad);
			
			mapNavigation = new MapNavigation();
			this.addChild(mapNavigation);
			mapNavigation.y = 665;
			
			mapNavigation.addEventListener(NavigationEvent.CHANGE_PAGE, onChangePage);
			
		}
		
		protected function onChangePage(event:NavigationEvent):void
		{
			displayContent(event.pageId);
			event.stopPropagation();
			
		}
		
		public function initContent():void
		{
			mapItems = ConfigManager.getInstance().mapItems;
		}
		
		private function setupDisplay():void
		{
			mapNavigation.mapItems = mapItems;
			pointsContainer.mapItems = mapItems;
			displayContent(0, true);
		}
		
		
		private function displayContent(value:int, isDirect:Boolean = false):void {
			
			var mapItemVO:MapItemVO = mapItems[value] as MapItemVO;
			mapNavigation.setActive(value);
			TweenLite.killTweensOf(pointsContainer);
			
			logoUI.gotoAndStop(value+1);

			
			
			TweenLite.to(pointsContainer, isDirect?0:0.4, {x: -stage.fullScreenWidth*value});
				
			
		}
		
		
		override protected function initSequence():void {
			displayContent(0, true);
		}
		
	}
}