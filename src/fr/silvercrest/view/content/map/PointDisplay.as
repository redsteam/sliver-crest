package fr.silvercrest.view.content.map
{
	import com.greensock.TweenLite;
	
	import flash.display.CapsStyle;
	import flash.display.JointStyle;
	import flash.display.LineScaleMode;
	import flash.display.MovieClip;
	import flash.display.Shape;
	import flash.geom.Point;
	import flash.text.TextField;
	import flash.text.TextFieldAutoSize;
	
	import fr.silvercrest.model.vo.PointVO;
	import fr.silvercrest.view.content.AutoHideSprite;
	
	public class PointDisplay extends AutoHideSprite
	{
		
		private var _pointVO:PointVO;
		private var targetPointUI:MovieClip;
		public var type:int;
		
		public function get pointVO():PointVO{
			return _pointVO;
		}
		
		public function set pointVO(value:PointVO):void {
			_pointVO = value;
			setupDisplay();
		}
		
		public function PointDisplay()
		{
			super();
			this.alpha = 0;
		}
		
		
		private function setupDisplay():void {
			
			switch (type) {
				
				case 1:
					targetPointUI = new Tip1UI();
				break;

				case 2:
					targetPointUI = new Tip2UI();
				break;
				
				default:
					targetPointUI = new BigTargetPointUI();
				break;
				
			}
			
			this.addChild(targetPointUI);
			targetPointUI.x = pointVO.targetX;
			targetPointUI.y = pointVO.targetY;

			
			
		}
		
		
		public function show():void {
			display(true);
		}
			
		public function hide():void {
			display(false);
		}
		
		protected function display(value:Boolean):void {
			
			TweenLite.to(this, 0.5, {alpha:value?1:0});
		}
	
		
	}
}