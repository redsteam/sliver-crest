package fr.silvercrest.view.content.components
{
	import com.greensock.TweenLite;
	
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.TouchEvent;
	import flash.geom.Point;
	
	import fr.silvercrest.view.content.navigation.NavigationButton;
	
	public class ToggleButton extends NavigationButton
	{
		public var toggleButtonUI:ToggleButtonUI;
		
		private var track:MovieClip;
		private var button:MovieClip;
		
		private var _selected:Boolean;
		private var _isDragging:Boolean;
		private var _previousMouseX:Number;
		
		public function set selected(value:Boolean):void {
			if (_selected!=value) {
				_selected = value;
				dispatchEvent(new Event(Event.CHANGE));
			}
			_selected = value;
			
			updatePosition();
		}
		
		
		
		private function updatePosition():void
		{
			var xTo:int = selected? track.width - button.width: 0;	
		
			TweenLite.to(button,0.3, {x:xTo});
		}
		
		public function get selected():Boolean {
			return _selected;
		}
		
		public function ToggleButton()
		{
			super();
			
			this.addEventListener(Event.ADDED_TO_STAGE,onAddedToStage);
		
			
		}
		
		
		protected function onAddedToStage(event:Event):void
		{
			this.removeEventListener(Event.ADDED_TO_STAGE,onAddedToStage);
			this.initChildren();
			setupTouchFunctions();
			
		}
		
		private function initChildren():void {
			
			toggleButtonUI = new ToggleButtonUI();
			this.addChild(toggleButtonUI);
			
			track = toggleButtonUI.track;
			button = toggleButtonUI.button;
			
		}
		
		private function setupTouchFunctions():void {
			
			toggleButtonUI.addEventListener(TouchEvent.TOUCH_TAP, onTap);
			
			button.addEventListener(TouchEvent.TOUCH_BEGIN, onThumbDown); 
			stage.addEventListener(TouchEvent.TOUCH_MOVE, onTouchMove); 
			stage.addEventListener(TouchEvent.TOUCH_END, onThumbUp); 
			
		}
		
		private function onThumbDown(event:TouchEvent) : void {
			_previousMouseX = event.stageX;
			_isDragging = true;
			TweenLite.killTweensOf(button);
			
		}
		
		private function onThumbUp(event:TouchEvent) : void {
			if (_isDragging) {
				_isDragging = false;
				
				replaceItem();
			}	
		}
		
		private function replaceItem():void
		{
			selected = button.x>= (track.width - button.width)/2;
			
		}
		
		private function normalizeX(value:int):int {
			
			if (value<0) {
				value = 0;
			} else if (value>track.width - button.width) {
				value = track.width - button.width;
			}
			return value;
		}
		
		private function onTouchMove(event:TouchEvent) : void {
			if (_isDragging) {
				
				var thisPosition:Point = localToGlobal(new Point(0,0));
				
				TweenLite.killTweensOf(button);
				var xTo:int = normalizeX(event.stageX-thisPosition.x);
				TweenLite.to(button, 0,{x:xTo});
				
				
			}
		}
		
		
		protected function onTap(event:TouchEvent):void
		{
			selected = !selected;
			
		}
	}
}