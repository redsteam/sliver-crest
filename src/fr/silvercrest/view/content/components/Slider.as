package fr.silvercrest.view.content.components
{
	import com.greensock.TweenLite;
	
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.TouchEvent;
	import flash.geom.Point;
	
	
	public class Slider extends Sprite
	{
		private var thumb:MovieClip;
		private var alphaPad:MovieClip;
		private var sliderUI:SliderUI;
		private var _isDragging:Boolean;
		
		private var thisPosition:Point;
		
		private var _percentValue:Number;
		
		public function Slider()
		{
			super();
			this.addEventListener(Event.ADDED_TO_STAGE,onAddedToStage);
		}
		
		public function setValue(value:Number):void {
		
			var yTo:int = normalizeY((1 - value)*alphaPad.height);
			TweenLite.killTweensOf(thumb);
			TweenLite.to(thumb, 0,{y:yTo});
			_percentValue = value;
		}
		
		public function set percentValue(value:Number):void {
		
			_percentValue = value;
			dispatchEvent(new Event(Event.CHANGE));
		}
		
		public function get percentValue():Number {
			return _percentValue; 
		}
		
		protected function onAddedToStage(event:Event):void
		{
			this.removeEventListener(Event.ADDED_TO_STAGE,onAddedToStage);
			this.initChildren();
			this.addListeners();
		}
		
		private function initChildren():void {
			sliderUI = new SliderUI();
			this.addChild(sliderUI);
			
			thumb = sliderUI.thumb;
			alphaPad = sliderUI.alphaPad;
			
		
		}
		
		private function addListeners():void {
		
			thumb.addEventListener(TouchEvent.TOUCH_BEGIN, onThumbDown); 
			alphaPad.addEventListener(TouchEvent.TOUCH_BEGIN, onPadDown); 
			stage.addEventListener(TouchEvent.TOUCH_END, onThumbUp); 
			stage.addEventListener(TouchEvent.TOUCH_MOVE, onTouchMove); 
			
		}
		
		private function normalizeY(value:int):int {
		
			if (value<0) {
				value = 0;
			} else if (value>alphaPad.height) {
				value = alphaPad.height;
			}
			return value;
		}
		
		protected function onTouchMove(event:TouchEvent):void
		{
			if (_isDragging) {
				thisPosition = localToGlobal(new Point(0,0));
				
				TweenLite.killTweensOf(thumb);
				var yTo:int = normalizeY(event.stageY-thisPosition.y);
				TweenLite.to(thumb, 0,{y:yTo});
				
				percentValue = 1 - yTo/alphaPad.height;
			
			}
		}
		
		protected function onThumbUp(event:TouchEvent):void
		{
			_isDragging = false;
			
		}
		
		protected function onPadDown(event:TouchEvent):void
		{
			event.stopPropagation();
			var yTo:int = normalizeY(event.localY*alphaPad.scaleY);
			TweenLite.killTweensOf(thumb);
			
			TweenLite.to(thumb, 0.4,{y:yTo});
			
			percentValue = 1 - yTo/alphaPad.height;
			_isDragging = true;
		}
		
		protected function onThumbDown(event:TouchEvent):void
		{
			event.stopPropagation();
			_isDragging = true;
			
		}
	}
}