package fr.silvercrest.view.content.videoSequence
{
	import com.greensock.TweenLite;
	
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.NetStatusEvent;
	import flash.events.StageVideoAvailabilityEvent;
	import flash.events.StageVideoEvent;
	import flash.geom.Rectangle;
	import flash.media.StageVideo;
	import flash.media.StageVideoAvailability;
	import flash.media.Video;
	import flash.net.NetConnection;
	import flash.net.NetStream;
	import flash.utils.clearTimeout;
	import flash.utils.setInterval;
	import flash.utils.setTimeout;
	
	import fr.silvercrest.constants.ApplicationConstants;
	import fr.silvercrest.model.events.VideoSequenceEvent;
	
	public class VideoSequence extends Sprite
	{
		private var introSequence:String;
		
		private var stageVideoAvail:Boolean;
		private var sv:StageVideo;
		private var ns:NetStream;
		
		private var stageWidth:int;
		private var stageHeight:int;
		
		private var _isPaused:Boolean;
		
		public static const VIDEO_STARTED:String = "VideoSequence.VIDEO_STARTED";
		public static const VIDEO_ENDED:String = "VideoSequence.VIDEO_ENDED";
		public static const GO_BACK_HOME:String = "VideoSequence.GO_BACK_HOME";
		public static const TIME_UPDATE:String = "VideoSequence.TIME_UPDATE";
		
		private var _hasdispatchedStart:Boolean;
		private var _hasdispatchedEnd:Boolean;
		
		private var _canDispatch:Boolean;
		
		private var nc:NetConnection;
		
		private var closeTimeout:int;
		
		private var _previousPlayHeadTime:Number = -1;
		
		public function VideoSequence()
		{
			super();
			this.alpha = 0;
			
			this.addEventListener(Event.ADDED_TO_STAGE, onAddedToStage);
			
			
		}
		
		public function get playHeadTime():Number {
			return _previousPlayHeadTime;
		}
		
		public function pauseVideo():void {
			try {
				_isPaused = true;
				ns.pause();
			} catch (e:Error){}
		}
		
		public function resumeVideo():void {
			try {
				_isPaused = false;

				ns.resume();
			} catch (e:Error){}
		}
		
		protected function onAddedToStage(event:Event):void
		{
			this.removeEventListener(Event.ADDED_TO_STAGE, onAddedToStage);
			
			stageWidth = stage.fullScreenWidth;
			stageHeight = stage.fullScreenHeight;
			
			if (stageHeight > stageWidth) {
				stageWidth = stageHeight;
				stageHeight = stage.fullScreenWidth;
				
			}
			
			
			stage.addEventListener(StageVideoAvailabilityEvent.STAGE_VIDEO_AVAILABILITY, onAvail);
			
		}
		
		private function onAvail(e:StageVideoAvailabilityEvent):void
		{
			stageVideoAvail = (e.availability == StageVideoAvailability.AVAILABLE);
			
			prepareNetStreams();
			
		}
		
		
		private function prepareNetStreams():void {
		
			nc = new NetConnection();
			nc.connect(null);
			
			
		
		}
		
		private function addUpdateListener():void {
			removeUpdateListener();
			_previousPlayHeadTime = -1;
			this.addEventListener(Event.ENTER_FRAME, onTimeUpdate);
		}
		private function removeUpdateListener():void {
			this.removeEventListener(Event.ENTER_FRAME, onTimeUpdate);
		}
		
		protected function onTimeUpdate(event:Event):void
		{
			if (ns){
				if (ns.time!= _previousPlayHeadTime){
				
					_previousPlayHeadTime = ns.time;
					dispatchEvent(new Event(TIME_UPDATE));
				}
			}
		}
		
		public function playNextStream(value:String):void {
			if (_isPaused) {
				playStream(value);
				return;
				
			}	
			addUpdateListener();
		
			ns.play(value);
			_hasdispatchedEnd = false;
			_canDispatch = true;
		}
		
		public function playStream(value:String):void {
			if (_isPaused) {
				ns.close();
				_isPaused = false;
			}
			
			addUpdateListener();
			_hasdispatchedStart = false;
			_hasdispatchedEnd = false;
			_canDispatch = true;
			clearTimeout(closeTimeout);
			ns = new NetStream(nc);
			ns.addEventListener(NetStatusEvent.NET_STATUS, onNetStatus,false, 0, true); 
			
			ns.client = this;
			
			
			if(stageVideoAvail)
			{
				sv = stage.stageVideos[0];
				sv.addEventListener(StageVideoEvent.RENDER_STATE, onRender);
				sv.attachNetStream(ns);
				trace('available');
			}
			else
			{
				var vid:Video = new Video(stageWidth, stageHeight);
				addChild(vid);
				vid.attachNetStream(ns);
				trace('not');
			}
			
			
			introSequence = value;
			trace ("playStream: "+introSequence);
			ns.play(value);
			/*
			if (value==introSequence) {
				ns.seek(0);
				ns.resume();
			} else {
				
				
				
			}
		*/
		}
		
		public function doDisplay(value:Boolean):void {
			TweenLite.to(this, 0, {alpha:value?1:0});
		}	
		
		public function show():void {
			doDisplay(true);
		}
		
		public function hide():void {
			doDisplay(false);
			_canDispatch = false;
		}
/*		
		override protected function initSequence():void {
			_hasdispatchedStart = false;
			
			prepareNetStreams();
			
			ns.play(introSequence);
		}
	*/	
		private function onNetStatus(event:Object):void {
			//handles NetConnection and NetStream status events
			trace (introSequence +" :: "+event.info.code);
			switch (event.info.code) {
				case "NetStream.Play.Start":
					ns.pause();
					//play stream if connection successful
					
					break;
				
				case "NetStream.Seek.Notify":
					ns.resume();
					
				break;
				
				case "NetStream.Unpause.Notify":
					if (!_hasdispatchedStart) {
						_hasdispatchedStart = true;
						setTimeout(dispatchStart, 200);
						_hasdispatchedEnd = false;
					}
					
				break;
				
				case "NetStream.Play.StreamNotFound":
					//error if stream file not found in
					//location specified
					trace("Stream not found: ");
					break;
				case "NetStream.Pause.Notify":
					//do if video is stopped
					if (!_isPaused) ns.seek(0);
					break;
			}
			//trace(event.info.code);
		}
		
		private function dispatchStart():void {
			
			trace ("dispatchStart: "+_canDispatch);
			
			if (_canDispatch)
			dispatchEvent(new Event(VIDEO_STARTED, true));
		}
		/*
		override protected function stopSequence():void {
		
			if (ns){
				ns.pause();
				ns.close();
			}
		}
		*/
		public function stopVideo():void {
			if (ns){
				trace ("stopVideo: "+introSequence);
				ns.pause();
				ns.close();
				removeUpdateListener();
				
			}
		}
		
		public function replay():void {
			if (ns){
				ns.seek(0);
				ns.resume();
			}
		}
		
		public function get currentTime():Number {
		
			if (ns) return ns.time;
			return 0;
		}
		
		private function onRender(e:StageVideoEvent):void
		{
			sv.viewPort = new Rectangle(0, 0, ApplicationConstants.APPLICATION_WIDTH, ApplicationConstants.INNER_CONTENT_HEIGHT);
			
		}
		
		public function onMetaData(e:Object):void
		{
			trace ("meta data!");
			
		}
		
		public function onXMPData(e:Object):void
		{
			
		}
		public function doCloseStream():void {
		
			ns.close();
		}
		
		public function onPlayStatus(e:Object):void
		{
			trace ("onPlayStatus" +e.code);
			if (e.code == 'NetStream.Play.Complete') {
			//	ns.seek(0);
			//	ns.resume();
			//	closeTimeout = setTimeout(doCloseStream, 200);
				
				if (!_hasdispatchedEnd && _canDispatch) {
					_hasdispatchedEnd = true;
					dispatchEvent(new Event(VIDEO_ENDED, true));
				}
			}
			
		}
		
	}
}