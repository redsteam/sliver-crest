package fr.silvercrest.view.content.videoSequence
{
	import com.greensock.TweenLite;
	
	import flash.events.Event;
	import flash.events.TouchEvent;
	import flash.text.TextFieldAutoSize;
	
	import fr.silvercrest.constants.ApplicationConstants;
	import fr.silvercrest.model.ConfigManager;
	import fr.silvercrest.model.events.NavigationEvent;
	import fr.silvercrest.model.vo.TextVO;
	import fr.silvercrest.model.vo.VideoVO;
	import fr.silvercrest.view.content.AbstractContent;
	
	public class VideoSequencer extends AbstractContent
	{
		private var _sequences:Array;
		
		private var videoSequence:VideoSequence;
		
		private var currentSequenceCounter:int = -1;
		
		private var _currentVideoVO:VideoVO;
		
		private var textMovieUI:TextMovieUI;
		private var _previousMouseX:Number;
		private var _isDragging:Boolean;
		
		private var _hasMoved:Boolean = false;
		
		private var alphaPad:alphaPadUI;
		
		private var pauseButton:PauseButtonUI;
		
		private var _isPaused:Boolean = false;
		
		public function get currentVideoVO():VideoVO {
			return _currentVideoVO;
		}
		public function set currentVideoVO(value:VideoVO):void {
			_currentVideoVO = value;
			setText('');
		}
		
		public function get sequences():Array{
			return _sequences;
		}
		public function set sequences(value:Array):void {
			_sequences = value;
		}
		
		
		public function VideoSequencer()
		{
			super();
			videoSequence = new VideoSequence();
			this.addChild(videoSequence);
		//	videoSequence.prepareNetStreams();
			videoSequence.doDisplay(true);
			
			videoSequence.addEventListener(VideoSequence.VIDEO_ENDED, onVideoEnded);
			videoSequence.addEventListener(VideoSequence.TIME_UPDATE, onTimeUpdate);
			
			
			textMovieUI = new TextMovieUI();
			this.addChild(textMovieUI);
			setText ('');
			
			pauseButton = new PauseButtonUI();
			this.addChild(pauseButton);
			pauseButton.visible = false;
			pauseButton.x = 28;
			pauseButton.y = 22;
			
			alphaPad = new alphaPadUI();
			this.addChild(alphaPad);
			alphaPad.width = ApplicationConstants.APPLICATION_WIDTH;
			alphaPad.height = ApplicationConstants.INNER_CONTENT_HEIGHT;
			
			setupPauseButtonDisplay();
			
			setupTouchFunctions();
		}
		
		private function setupTouchFunctions() : void {
			
			
			alphaPad.addEventListener(TouchEvent.TOUCH_BEGIN, onThumbDown); 
		}
		
		protected function togglePause():void
		{
			_isPaused = !_isPaused;
			
			if (_isPaused) {
				videoSequence.pauseVideo();
			} else {
				videoSequence.resumeVideo();
				}
			setupPauseButtonDisplay();
		}
		
		private function setupPauseButtonDisplay():void {
			pauseButton.visible = _isPaused;
		}
		
		
		private function onThumbDown(event:TouchEvent) : void {
			if (!_isActive) return;
			
			stage.addEventListener(TouchEvent.TOUCH_MOVE, onTouchMove); 
			stage.addEventListener(TouchEvent.TOUCH_END, onThumbUp); 
			
			
			
			_previousMouseX = event.stageX;
			_isDragging = true;
			_hasMoved = false;
			
		}
		
		private function onThumbUp(event:TouchEvent) : void {
			if (!_isActive) return;
			stage.removeEventListener(TouchEvent.TOUCH_MOVE, onTouchMove); 
			stage.removeEventListener(TouchEvent.TOUCH_END, onThumbUp); 
			
			
			//	if (_isDragging)_justReleased = true;
			if (_isDragging) {
				_isDragging = false;
				
			}	
			
			if (!_hasMoved) {
				togglePause();
			
			} 
			
		}
		
		private function onTouchMove(event:TouchEvent) : void {
			if (!_isActive) return;
			if (_isDragging) {

				var moveTo:int =  (event.stageX - _previousMouseX);
				_previousMouseX = event.stageX;

				_hasMoved = true;
				//trace (moveTo);
				
				if (Math.abs(moveTo)<50)return;
				
				
				if (moveTo<0) {
					// go next if available
					if (currentSequenceCounter<sequences.length-1) {
						_isDragging = false;
						goNext();
					}
				} else if (moveTo>0){
					// go back if available
					goBack();
					_isDragging = false;
				
				}
			}
		}
		
		
		private var _currentTextVO:TextVO;
		
		protected function onTimeUpdate(event:Event):void
		{
		
			var tmpTextVO:TextVO = getCurrentTextDisplay(videoSequence.playHeadTime);
			if (_currentTextVO!=tmpTextVO) {
				_currentTextVO = tmpTextVO;
				if (_currentTextVO) {
					setText(_currentTextVO.text)
				} else {
					setText('');
				}
			}
		}
		
		
		//-------------------------------------------------------------
		private function setText(value:String):void {
			
			textMovieUI.captionText.htmlText = value;
			textMovieUI.captionText.autoSize = TextFieldAutoSize.CENTER;
			
			textMovieUI.y = Math.round(ApplicationConstants.INNER_CONTENT_HEIGHT - textMovieUI.height - 10);
			
		}
		
		//-------------------------------------------------------------
		private function getCurrentTextDisplay(time:Number):TextVO {
			
			var lng:int = currentVideoVO.texts.length;
			var textVO:TextVO;
			for (var i : int = 0; i < lng; i++) {
				textVO = currentVideoVO.texts[i] as TextVO;
				if ((textVO.startAt<= time) && ((textVO.startAt + textVO.duration)>=time)) {
					return textVO;
				}
				
			}
			
			return null;
			
		}
		
		
		protected function onVideoEnded(event:Event):void
		{
			
			trace ("onVideoEnded: "+ConfigManager.getInstance().autoPlay);
			
			if (ConfigManager.getInstance().autoPlay && currentSequenceCounter<sequences.length-1) {
				currentSequenceCounter++;
				if (currentSequenceCounter<sequences.length) {
					currentVideoVO = _sequences[currentSequenceCounter] as VideoVO;
					videoSequence.playNextStream(currentVideoVO.file);
					_isPaused = false;
					setupPauseButtonDisplay();

				}
				
				checkNextPrevStatus();
			}
			
		}
		
		override protected function initSequence():void {
		
			currentSequenceCounter = 0;
			currentVideoVO = _sequences[currentSequenceCounter] as VideoVO;
			
			videoSequence.playStream(currentVideoVO.file);
			
			_isPaused = false;
			setupPauseButtonDisplay();

			checkNextPrevStatus();
		
		}
		
		private function checkNextPrevStatus():void {
		
			if (currentSequenceCounter<sequences.length-1) {
				dispatchEvent(new NavigationEvent(NavigationEvent.SHOW_NEXT, true));
			} else {
				dispatchEvent(new NavigationEvent(NavigationEvent.HIDE_NEXT, true));
			}
		}
		
		override protected function stopSequence():void {
			videoSequence.stopVideo();
		}

		override public function goBack():void {
			
			
			// check currentTime
			
			if (videoSequence.currentTime<1) {
				
				videoSequence.stopVideo();
				
				currentSequenceCounter--;
				if (currentSequenceCounter<=-1) {
					// go back home!
					dispatchEvent(new NavigationEvent(NavigationEvent.GO_HOME, true));
					currentSequenceCounter = -1;
				} else {
					currentVideoVO = _sequences[currentSequenceCounter] as VideoVO;
					_isPaused = false;
					setupPauseButtonDisplay();

					videoSequence.playNextStream(currentVideoVO.file);
					checkNextPrevStatus();
		
				}
				
			} else {
				// replay same video
				videoSequence.replay();
				_isPaused = false;
				setupPauseButtonDisplay();

			}
			
		}
		
		override public function goNext():void {
			videoSequence.stopVideo();
			currentSequenceCounter++;
			if (currentSequenceCounter<sequences.length) {
				currentVideoVO = _sequences[currentSequenceCounter] as VideoVO;
				videoSequence.playNextStream(currentVideoVO.file);
				_isPaused = false;
				setupPauseButtonDisplay();

			}
			
			checkNextPrevStatus();

			
		}

	}
}