package fr.silvercrest.view.content.navigation
{
	import flash.display.MovieClip;

	public class BtnNext extends NavigationButton
	{
		
		
		private var ui:MovieClip;
		
		public function BtnNext()
		{
			super();
			ui = new BtnNextUI();
			this.addChild(ui);
		}
	}
}