package fr.silvercrest.view.content.navigation
{
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.TouchEvent;
	
	import fr.silvercrest.constants.ApplicationConstants;
	import fr.silvercrest.model.ConfigManager;
	import fr.silvercrest.model.events.NavigationEvent;
	import fr.silvercrest.view.content.AbstractContent;
	import fr.silvercrest.view.content.components.ToggleButton;
	
	public class NavigationBar extends AbstractContent
	{
		private var navigationBarUI:NavigationBarUI;
		
		private var btnHome:BtnHome;
		private var btnBack:BtnBack;
		private var btnNext:BtnNext;
		
		private var toggleButton:ToggleButton;
		
		public function NavigationBar()
		{
			super();
			navigationBarUI = new NavigationBarUI();
			this.addChild(navigationBarUI);
			
			btnHome = new BtnHome();
			btnBack = new BtnBack();
			btnNext = new BtnNext();
			
			
			btnHome.x = 16;
			btnBack.x = 85;
			btnNext.x = 139;
			
			btnHome.y = btnBack.y = btnNext.y = 5;
			
			this.addChild(btnHome);
			this.addChild(btnBack);
			this.addChild(btnNext);
			
			btnHome.addEventListener(TouchEvent.TOUCH_TAP, onGoHome);
			btnBack.addEventListener(TouchEvent.TOUCH_TAP, onGoBack);
			btnNext.addEventListener(TouchEvent.TOUCH_TAP, onGoNext);
			
			
			toggleButton = new ToggleButton();
			navigationBarUI.addChild(toggleButton);
			toggleButton.x = 473;
			toggleButton.y = 8;
			
			toggleButton.addEventListener(Event.CHANGE, onToggleChange);
			
			setState(ApplicationConstants.CONTENT_HOME);
		}
		
		protected function onToggleChange(event:Event):void
		{
			
			ConfigManager.getInstance().autoPlay = toggleButton.selected;
		}
		
		protected function onGoNext(event:TouchEvent):void
		{
			dispatchEvent(new NavigationEvent(NavigationEvent.GO_NEXT));
			
		}
		
		protected function onGoBack(event:TouchEvent):void
		{
			dispatchEvent(new NavigationEvent(NavigationEvent.GO_BACK));
			
		}
		
		
		
		protected function onGoHome(event:TouchEvent):void
		{
			dispatchEvent(new NavigationEvent(NavigationEvent.GO_HOME));
			
		}
		
		public function showNext(value:Boolean):void {
			btnNext.display(value);
			btnHome.display(true);
			btnBack.display(true);
			toggleButton.display(false);
			
		}
		
		
		public function setState(value:String):void {
			
			
			btnNext.display(false);
			btnHome.display(false);
			btnBack.display(false);
			
			
			switch(value)
			{
				case ApplicationConstants.CONTENT_MOSCOW:
				case ApplicationConstants.CONTENT_NASSAU:
				{
					this.show();
					btnNext.display(true);
					btnHome.display(true);
					btnBack.display(true);
					toggleButton.display(true);
					break;
				}
					
				case ApplicationConstants.CONTENT_PLANE:
				{
					this.show();
					btnNext.display(true);
					btnHome.display(true);
					btnBack.display(true);
					toggleButton.display(false);

					break;
				}
				
				case ApplicationConstants.CONTENT_HOME:
					this.hide();
					toggleButton.display(true);
				
				break;	
					
				default:
				{
					
					break;
				}
			}
			
		}
	}
}