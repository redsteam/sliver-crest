package fr.silvercrest.view.content.navigation
{
	import com.greensock.TweenLite;
	
	
	import fr.silvercrest.view.content.AutoHideSprite;
	
	public class NavigationButton extends AutoHideSprite
	{
		public function NavigationButton()
		{
			super();
		}
		
		
		public function display(value:Boolean):void {
			TweenLite.killTweensOf(this);
			TweenLite.to(this, 0.5, {alpha:value?1:0});
		}
		
		
	}
}