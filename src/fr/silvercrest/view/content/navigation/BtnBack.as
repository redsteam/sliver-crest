package fr.silvercrest.view.content.navigation
{
	import flash.display.MovieClip;

	public class BtnBack extends NavigationButton
	{
		private var ui:MovieClip;
		
		public function BtnBack()
		{
			super();
			ui = new BtnBackUI();
			this.addChild(ui);
		}
	}
}