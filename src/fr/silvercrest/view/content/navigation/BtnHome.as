package fr.silvercrest.view.content.navigation
{
	import flash.display.MovieClip;

	public class BtnHome extends NavigationButton
	{
		
		private var ui:MovieClip;
		
		public function BtnHome()
		{
			super();
			ui = new BtnHomeUI();
			this.addChild(ui);
		}
		
	}
}