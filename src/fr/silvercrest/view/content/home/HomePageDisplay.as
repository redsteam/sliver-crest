package fr.silvercrest.view.content.home
{
	import com.greensock.TweenLite;
	
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.TouchEvent;
	import flash.ui.Multitouch;
	import flash.ui.MultitouchInputMode;
	
	import fr.silvercrest.constants.ApplicationConstants;
	import fr.silvercrest.model.ConfigManager;
	import fr.silvercrest.model.events.NavigationEvent;
	import fr.silvercrest.view.content.AbstractContent;
	
	public class HomePageDisplay extends AbstractContent
	{
		
		private var homePageImageRotation:HomePageImageRotation;
		
		private const FRAMES_TARGET:Array = [36, 92, 118];
		
		private var _previousMouseX:Number;
		private var _isDragging:Boolean;
		public var autoPlay:Boolean = true;
		private var moveSpeed:int = 0;
		private var normalizer:int = 10;
		private var endTween:TweenLite;
		private var foreVisionLogo:ForeVisionLogo;
		private var _finalTarget:int;
		private var _justReleased:Boolean = false;
		private var _hasSetListeners:Boolean = false;
		
		public function HomePageDisplay()
		{
			super();
			
			homePageImageRotation = new HomePageImageRotation();
			this.addChild(homePageImageRotation);
			finalTarget = FRAMES_TARGET[1];
			
			foreVisionLogo = new ForeVisionLogo();
			this.addChild(foreVisionLogo);
			foreVisionLogo.x = ApplicationConstants.APPLICATION_WIDTH/2;
			
			setTouchZoneListeners(true);
			
			Multitouch.inputMode = MultitouchInputMode.TOUCH_POINT;
			
			
			
			setupTouchFunctions();
			
			this.addEventListener(Event.ENTER_FRAME, onEnterFrame);
			
		}
		
		override protected function display(value:Boolean):void {
			
			_isActive = value;
			
			TweenLite.to(this, 0, {alpha:1});
			
			if (value) {
				initSequence();
			} else {
				stopSequence();
			}
			
		}
		
		public function forceHide():void {
			TweenLite.to(this, 0, {alpha:0});
			
		}
		
		private function onActionClicked(event:TouchEvent):void {
			if (!_isActive) return;
			event.stopPropagation();
			
			var pageId:int =0;
			var lng:int = FRAMES_TARGET.length;
			for (var i:int = 0; i < lng; i++) 
			{
				if (FRAMES_TARGET[i]==homePageImageRotation.currentFrame) {
					pageId = i;
					break;
				}
			}
			
			
			dispatchEvent(new NavigationEvent(NavigationEvent.CHANGE_PAGE, true, false, pageId));
			
		}
		
		private function setTouchZoneListeners(value:Boolean):void {
			
		//	trace ("setTouchZoneListeners: "+value);
			
			if (_hasSetListeners==value) return;
			_hasSetListeners = value;
			
			try {
				homePageImageRotation.touchZone.removeEventListener(TouchEvent.TOUCH_TAP, onActionClicked);
				
				if (value) {
					homePageImageRotation.touchZone.addEventListener(TouchEvent.TOUCH_TAP, onActionClicked); 
				}			
			} catch (e:Error) {
			
				//trace ("unable to "+(value?'':'un')+"set listeners");
			}
		}
		
		public function get finalTarget():int {
			return _finalTarget;
		}
		
		public function set finalTarget(value:int):void {
			_finalTarget= value;
			homePageImageRotation.gotoAndStop(value);
		}
		
		private function setupTouchFunctions() : void {
			
			
			this.addEventListener(TouchEvent.TOUCH_BEGIN, onThumbDown); 
			
			
			if (stage) {
				addStageListeners();
			} else {
				this.addEventListener(Event.ADDED_TO_STAGE, onAddedToStage);
			}
		}
		
		private function onAddedToStage(event:Event) : void {
			addStageListeners();
			this.removeEventListener(Event.ADDED_TO_STAGE, onAddedToStage);
		}
		
		private function addStageListeners() : void {
			
			stage.addEventListener(TouchEvent.TOUCH_MOVE, onTouchMove); 
			stage.addEventListener(TouchEvent.TOUCH_END, onThumbUp); 
			
			
		}
		
		private function onThumbDown(event:TouchEvent) : void {
			if (!_isActive) return;
			_previousMouseX = event.stageX;
			autoPlay = false;
			_isDragging = true;
			

		}
		
		private function onThumbUp(event:TouchEvent) : void {
			if (!_isActive) return;
			if (_isDragging)_justReleased = true;
			_isDragging = false;
			
		}
		
		private function onTouchMove(event:TouchEvent) : void {
			if (!_isActive) return;
			if (_isDragging) {
				dispatchValueChanged(event);
				setTouchZoneListeners(false);
			}
		}
		
		private function dispatchValueChanged(event:TouchEvent) : void {
			
			
			moveSpeed = -(event.stageX - _previousMouseX);
			
			
			_previousMouseX = event.stageX;
			
		}
		
		
		private function onEnterFrame(event:Event) : void {
			
				if (!_isActive) return;
				// erode moveSpeed
				
				var inertie:int = 5;
				
				if (moveSpeed>0){
					
					if (Math.round(moveSpeed - moveSpeed/inertie)== moveSpeed) {
						moveSpeed = 0;
					} else {
						moveSpeed -= moveSpeed/inertie;
					}
					
					if (moveSpeed < 0) moveSpeed = 0;
				} else {
					if (Math.round(moveSpeed + moveSpeed/inertie)== moveSpeed) {
						moveSpeed = 0;
					} else {
						moveSpeed+= -moveSpeed/inertie;
					}
					if (moveSpeed > 0) moveSpeed = 0;
				}
				
				var targetFrame:int;
				
					targetFrame = (homePageImageRotation.currentFrame - Math.round(moveSpeed/normalizer));//
					if (targetFrame > homePageImageRotation.totalFrames) {
						targetFrame = homePageImageRotation.totalFrames;
						moveSpeed = -moveSpeed;
					}
					
					if (targetFrame<1) {	
						targetFrame = 1;
						moveSpeed = -moveSpeed;
					}
					
					
				if (_justReleased) {
					// we should go to the nearest frame
					_justReleased = false;
					
					var targetPoint:int = (moveSpeed<0) ? FRAMES_TARGET[FRAMES_TARGET.length-1]: FRAMES_TARGET[0];
					
					
					var lng:int = FRAMES_TARGET.length;
					
					if (moveSpeed ==0) {
						var pointDiff:int = 2000;
						for (var i:int = 0; i < lng; i++) 
						{
							var thisDiff:int = Math.abs(FRAMES_TARGET[i]-homePageImageRotation.currentFrame);
							if (thisDiff<pointDiff){
								pointDiff = thisDiff;
								targetPoint = FRAMES_TARGET[i];
							}
						
						}
					}
					var currentPoint:int;
					var hasFound:Boolean = false;
					
				//	trace ("targetFrame: "+targetFrame +" currentFrame: "+homePageImageRotation.currentFrame+" moveSpeed: "+moveSpeed);
					
					var isAlreadyThere:Boolean = false;
					if (moveSpeed==0) {
						
					
						// are we already on a hot spot?
						for (var i:int = 0; i < lng; i++) 
						{
							currentPoint = FRAMES_TARGET[i];
							if (currentPoint == targetFrame){
								isAlreadyThere = true;
								break;
							}
						}
					}
					
					if (isAlreadyThere) {
						homePageImageRotation.gotoAndStop(targetFrame);
						_finalTarget = targetFrame;
						
						setTouchZoneListeners(true);
					} else {
					
						for (var i:int = 0; i < lng; i++) 
						{
							currentPoint = FRAMES_TARGET[i];
							
							// are we going forward?
							if (moveSpeed<=0) {
								// target point is next bigger point
								if (currentPoint>homePageImageRotation.currentFrame) {
									targetPoint = currentPoint;
									hasFound = true;
								}
							} else {
								// target point is next smaller
								
								if (currentPoint<homePageImageRotation.currentFrame && targetPoint<currentPoint) {
									targetPoint = currentPoint;
								}
								
							}
							
							
							if (hasFound) break;
						} 
						
						
						moveSpeed = 0;
						TweenLite.killTweensOf(this);
						
						var tweenSpeed:Number = Math.abs(targetPoint - homePageImageRotation.currentFrame) <10 ? 0.2 :0.4;
					//	trace ("targetPoint: "+targetPoint +" :: "+tweenSpeed);
						
						TweenLite.to(this, tweenSpeed, {finalTarget: targetPoint, onComplete:setTouchZoneListeners, onCompleteParams:[true] });
					}
					
				}	else {
					homePageImageRotation.gotoAndStop(targetFrame);
					_finalTarget = targetFrame;
				//	trace ("targetFrame: "+targetFrame + " moveSpeed: "+moveSpeed);
					
				}
					
				
				
			
		}
		
	}
}