package fr.silvercrest.view.content.plane
{
	import com.greensock.TweenLite;
	
	import flash.display.MovieClip;
	import flash.events.Event;
	import flash.events.TouchEvent;
	import flash.utils.clearTimeout;
	import flash.utils.setTimeout;
	
	import fr.silvercrest.constants.ApplicationConstants;
	import fr.silvercrest.model.ConfigManager;
	import fr.silvercrest.model.events.NavigationEvent;
	import fr.silvercrest.view.content.AbstractContent;
	import fr.silvercrest.view.content.components.Slider;
	import fr.silvercrest.view.content.videoSequence.VideoSequence;
	
	public class AnimReactor extends AbstractContent
	{
		private var bkg:AnimReactorBkg;
		private var slider:Slider;
		private var motor:MovieClip;
		private var pointsDisplayer:PointsDisplayer;
		private var _isInRotation:Boolean;
		
		private var rotationSequence:VideoSequence;
		private var closeTimeout:int;
		
		private var rotationStartTiemout:int;
		
		private var btnRotation:BtnRotationUI;
		
		private var _isInChange:Boolean;
		
		public function AnimReactor()
		{
			super();
			tweenSpeed = 0.5;
			this.addEventListener(Event.ADDED_TO_STAGE,onAddedToStage);
		}
		
		protected function onAddedToStage(event:Event):void
		{
			this.removeEventListener(Event.ADDED_TO_STAGE,onAddedToStage);
			this.initChildren();
		}
		
		
		private function initChildren():void {
			
			rotationSequence = new VideoSequence();
			this.addChild(rotationSequence);
			rotationSequence.addEventListener(VideoSequence.VIDEO_STARTED,onVideoStarted);
			rotationSequence.addEventListener(VideoSequence.VIDEO_ENDED,onVideoEnded);
			
			
			bkg = new AnimReactorBkg();
			this.addChild(bkg);
			
			
			motor = new MotorUI();
			this.addChild(motor);
			
			
			motor.x = 132;
			motor.y = 180; // 145
			
			
			pointsDisplayer = new PointsDisplayer();
			this.addChild(pointsDisplayer);
			
			
			slider = new Slider();
			this.addChild(slider);
			
			slider.x = 970;
			slider.y = 60;
			
			slider.addEventListener(Event.CHANGE, onSliderChange);
			
			slider.setValue(0);
			updateMotorDisplay(0, true);
			
			
			btnRotation = new BtnRotationUI();
			this.addChild(btnRotation);
			
			btnRotation.x = 771;
			btnRotation.y = 650;
			
			btnRotation.addEventListener(TouchEvent.TOUCH_TAP, onRotationTap);
			
		}
		
		protected function onRotationTap(event:TouchEvent):void
		{
			event.stopPropagation();
			if (_isInChange) return;
			if (!_isInRotation) {
				startRotation();
			} else {
				onEndVideo();
			}		
		}
		override protected function stopSequence():void {
			trace ("do stop sequence: _isInRotation"+_isInRotation);
			clearTimeout(rotationStartTiemout);
			rotationSequence.hide();
			rotationSequence.stopVideo();
			onEndVideo();
		}
		
		private function doCloseStream():void {
			rotationSequence.doCloseStream();
			
		}
		
		private function onEndVideo():void {
			trace ("onEndVideo"+_isInRotation);
			
			if (!_isInRotation) return;
			closeTimeout = setTimeout(doCloseStream,200);
			_isInChange = true;
			bkg.visible = true;
			motor.visible = true;
			slider.visible = true;
			pointsDisplayer.visible = true;
			
			_isInRotation = false;
			
			TweenLite.killTweensOf(slider);
			TweenLite.killTweensOf(pointsDisplayer);
			TweenLite.killTweensOf(bkg.titles);
//			TweenLite.killTweensOf(btnRotation);
			
			// we hide the points and slider
//			TweenLite.to(btnRotation, 0, {alpha:0});
			TweenLite.to(slider, 0, {alpha:0});
			TweenLite.to(pointsDisplayer, 0, {alpha:0});
			TweenLite.to(bkg.titles, 0, {alpha:0});
			
			setTimeout(revertToSlider, 500);
			
		
		
		}
		
		private function revertToSlider():void {
//			TweenLite.to(btnRotation, 0.5, {alpha:1});
			TweenLite.to(slider, 0.5, {alpha:1});
			TweenLite.to(pointsDisplayer, 0.5, {alpha:1});
			TweenLite.to(bkg.titles, 0.5, {alpha:1});
			
			updateMotorDisplay(slider.percentValue);
			_isInChange = false;
		}
		
		protected function onVideoEnded(event:Event):void
		{
			
			
			event.stopPropagation();

			rotationSequence.hide();
			onEndVideo();
			
		}
		
		protected function onVideoStarted(event:Event):void
		{
			_isInChange = false;
			
			bkg.visible = false;
			motor.visible = false;
			slider.visible = false;
			pointsDisplayer.visible = false;
		}
		
		public function initContent():void
		{
			pointsDisplayer.points = ConfigManager.getInstance().motorsPoint;
			pointsDisplayer.setValue(0);
		}
		
		protected function onSliderChange(event:Event):void
		{
			
			updateMotorDisplay(slider.percentValue);
			pointsDisplayer.setValue(slider.percentValue);
			//trace ("onSliderChange: "+slider.percentValue);
		}
		
		private function updateMotorDisplay(value:Number, isDirect:Boolean = false):void {
			
			var tmpVal:Number = 1-value;
			
			var hueTo:Number = 177 * tmpVal;
			var saturationTo:Number = -0.91 * tmpVal + 1;
			var brightnessTo:Number = -0.52 * tmpVal + 1;
			
		//	trace("hueTo: "+hueTo +" :: "+saturationTo +" :: "+brightnessTo);
			TweenLite.killTweensOf(motor);
			
			TweenLite.to(motor, isDirect?0:0.5, {colorMatrixFilter:{hue:hueTo,saturation: saturationTo, brightness:brightnessTo }}); 
		
			TweenLite.to(bkg.titles.subtitle, 0.5, {alpha:value>0.95?1:0});
			
		}
		
		private function startRotation():void {
			
			if (!_isInRotation) {
				_isInChange = true;
				_isInRotation = true;
				clearTimeout(closeTimeout);
				// we hide the points and slider
				TweenLite.killTweensOf(slider);
				TweenLite.killTweensOf(pointsDisplayer);
				TweenLite.killTweensOf(bkg.titles);
//				TweenLite.killTweensOf(btnRotation);
				
				
//				TweenLite.to(btnRotation, 0.5, {alpha:0});
				TweenLite.to(slider, 0.5, {alpha:0});
				TweenLite.to(pointsDisplayer, 0.5, {alpha:0});
				TweenLite.to(bkg.titles, 0.5, {alpha:0});
				updateMotorDisplay(1);
				
				rotationStartTiemout = setTimeout(displayMovieRotation, 500);
				
			}
		}
		private function displayMovieRotation():void {
			
			trace ("displayMovieRotation");
			
			rotationSequence.show();
			rotationSequence.playStream(ApplicationConstants.MOTOR_360_MOVIE);
			
		}
		
		override protected function initSequence():void {
			slider.setValue(0);
			updateMotorDisplay(0, true);
			pointsDisplayer.setValue(0, true);
			
			
		}

		
	}
}