package fr.silvercrest.view.content.plane
{
	import com.greensock.TweenLite;
	
	import flash.display.CapsStyle;
	import flash.display.JointStyle;
	import flash.display.LineScaleMode;
	import flash.display.Shape;
	import flash.geom.Point;
	import flash.text.TextField;
	import flash.text.TextFieldAutoSize;
	
	import fr.silvercrest.model.vo.PointVO;
	import fr.silvercrest.view.content.AutoHideSprite;
	
	public class PointDisplay extends AutoHideSprite
	{
		
		private var _pointVO:PointVO;
		private var targetPointUI:TargetPointUI;
		private var tooltipBoxUI:TooltipBoxUI;
		private var captionText:TextField;
		
		public function get pointVO():PointVO{
			return _pointVO;
		}
		
		public function set pointVO(value:PointVO):void {
			_pointVO = value;
			setupDisplay();
		}
		
		public function PointDisplay()
		{
			super();
			this.alpha = 0;
		}
		
		
		private function setupDisplay():void {
		
			targetPointUI = new TargetPointUI();
			this.addChild(targetPointUI);
			targetPointUI.x = pointVO.targetX;
			targetPointUI.y = pointVO.targetY;

			tooltipBoxUI = new TooltipBoxUI();
			this.addChild(tooltipBoxUI);
			captionText = tooltipBoxUI.captionText;
			
			captionText.htmlText = pointVO.caption;
			captionText.autoSize = TextFieldAutoSize.LEFT;
			
			
			tooltipBoxUI.x = pointVO.legendX - captionText.width/2;
			tooltipBoxUI.y = pointVO.legendY;
			
			
			// draw the curve
			var endDot:Point = new Point(pointVO.targetX, pointVO.targetY);
			var startDot:Point = new Point(pointVO.legendX, pointVO.legendY);
			var controlDot:Point = new Point();
			
			var halfWayX:Number;
			var halfWayY:Number;
			
			if (startDot.x < endDot.x)
			{
				halfWayX = startDot.x + ((endDot.x - startDot.x)/2);
				halfWayY = startDot.y + ((endDot.y - startDot.y)/2);
			}
			else if (startDot.x > endDot.x)
			{
				halfWayX = endDot.x + ((startDot.x - endDot.x)/2);
				halfWayY = startDot.y + ((endDot.y - startDot.y)/2);
			}
			
			var dx:Number = endDot.x - startDot.x;
			var dy:Number = endDot.y - startDot.y;
			
			
			var distanceBetweenPoints:Number = Math.sqrt( dx * dx + dy * dy );
			var percentageOfDistance:int =70;  //This is the percentage of the distance between the points that you want to add; it can be anything from 0 to 100.
			
			var amountToAdd:Number = (distanceBetweenPoints * percentageOfDistance) / 100;
			//trace(distanceBetweenPoints, amountToAdd);
			halfWayY -= amountToAdd;		
			
			controlDot.x = halfWayX;
			controlDot.y = halfWayY;
			
			var lineShape:Shape = new Shape();
			
			
			lineShape.graphics.lineStyle(1, 0x8B8D92, 1, false, LineScaleMode.NORMAL,
				CapsStyle.ROUND, JointStyle.ROUND, 10);

			
			lineShape.graphics.moveTo(pointVO.legendX, pointVO.legendY);
			lineShape.graphics.curveTo(controlDot.x, controlDot.y, pointVO.targetX, pointVO.targetY);
			lineShape.graphics.endFill();
			
			this.addChild(lineShape);
			
			/*
			
			
			
			*/
			
		}
		
		
		public function show(isDirect:Boolean = false):void {
			display(true, isDirect);
		}
			
		public function hide(isDirect:Boolean = false):void {
			display(false, isDirect);
		}
		
		protected function display(value:Boolean, isDirect:Boolean = false):void {
			
			TweenLite.to(this, isDirect?0:0.5, {alpha:value?1:0});
		}
	
		
	}
}