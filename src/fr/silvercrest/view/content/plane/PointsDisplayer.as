package fr.silvercrest.view.content.plane
{
	import flash.display.Sprite;
	
	import fr.silvercrest.model.vo.PointVO;
	import fr.silvercrest.view.content.AutoHideSprite;
	
	public class PointsDisplayer extends Sprite
	{
		
		private var _points:Array;
		
		public function set points(value:Array):void {
			_points = value;
			initContent();
		}
		
		public function get points():Array{
			return _points;
		}
		
		
		public function PointsDisplayer()
		{
			super();
		}
		
		private function initContent():void {
		
		
			var lng:int = points.length;
			var pointDisplay:PointDisplay;
			
			for (var i:int = 0; i < lng; i++) 
			{
				pointDisplay = new PointDisplay();
				this.addChild(pointDisplay);
				pointDisplay.pointVO = points[i] as PointVO;
				
			}
			
		}
		
		
		public function setValue(value:Number, isDirect:Boolean = false):void {
			
			// display or hide points depending on their value
			var lng:int = this.numChildren;
			var pointDisplay:PointDisplay;
			
			for (var i:int = 0; i < lng; i++) 
			{
				pointDisplay = this.getChildAt(i) as PointDisplay;
				if (pointDisplay.pointVO.displayAt<=value){
					pointDisplay.show(isDirect);
				} else {
					pointDisplay.hide(isDirect);
				}
			}
			
		}
	}
}