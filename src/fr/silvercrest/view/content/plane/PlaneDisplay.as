package fr.silvercrest.view.content.plane
{
	import flash.events.Event;
	
	import fr.silvercrest.model.ConfigManager;
	import fr.silvercrest.model.events.NavigationEvent;
	import fr.silvercrest.view.content.AbstractContent;
	import fr.silvercrest.view.content.map.MapAnimation;
	import fr.silvercrest.view.content.menuExt.MenuExtAnim;
	import fr.silvercrest.view.content.operation.OperationAnimation;
	
	public class PlaneDisplay extends AbstractContent
	{
		
		public var menuExtAnim:MenuExtAnim;
		public var animReactor:AnimReactor;
		public var mapAnimation:MapAnimation;
		public var operationAnimation:OperationAnimation;
		
		private var sequences:Array;
		
		public var currentSequenceCounter:int = -1;
		
		private var currentContent:AbstractContent;
		
		public function PlaneDisplay()
		{
			super();
			
			initChildren();
		}
		
		private function initChildren():void {
		
			menuExtAnim = new MenuExtAnim();
			this.addChild(menuExtAnim);
			
			animReactor = new AnimReactor();
			this.addChild(animReactor);
			
			mapAnimation = new MapAnimation();
			this.addChild(mapAnimation);
			
			operationAnimation = new OperationAnimation();
			this.addChild(operationAnimation);
			
			sequences = new Array();
			sequences.push(menuExtAnim);
			sequences.push(animReactor);
			sequences.push(mapAnimation);
			sequences.push(operationAnimation);
			
			menuExtAnim.addEventListener(NavigationEvent.CHANGE_INNER_PAGE, onChangeInnerPage);
			
		}
		
		protected function onChangeInnerPage(event:NavigationEvent):void
		{
			event.stopPropagation();
			displayContentNum(event.pageId);
			
		
			
		}
		public function displayContentNum(value:int) :void {
		
			currentSequenceCounter = value;
			
			if (currentContent)currentContent.hide();
			currentContent = sequences[currentSequenceCounter];
			currentContent.show();
			
			
			checkNextPrevStatus();
		}
		
		
		public function initContent():void
		{
			menuExtAnim.initContent();
			animReactor.initContent();
			mapAnimation.initContent();
			operationAnimation.initContent();
			
		}
	
		override protected function initSequence():void {
			
			currentSequenceCounter = 0;
			if (currentContent)currentContent.hide();
			currentContent = sequences[currentSequenceCounter];
			currentContent.show();
			
			
			checkNextPrevStatus();
			
		}
		

		
		private function checkNextPrevStatus():void {
			
		
			if (currentSequenceCounter<sequences.length-1) {
				dispatchEvent(new NavigationEvent(NavigationEvent.SHOW_NEXT, true));
			} else {
				dispatchEvent(new NavigationEvent(NavigationEvent.HIDE_NEXT, true));
			}
		}
		
		override protected function stopSequence():void {
			if (currentContent)currentContent.hide();
		}

		
		
		
		override public function goBack():void {
			if (currentContent)currentContent.hide();
			
			currentSequenceCounter--;
			if (currentSequenceCounter<=-1) {
				// go back home!
				dispatchEvent(new NavigationEvent(NavigationEvent.GO_HOME, true));
				currentSequenceCounter = -1;
			} else {
				
				currentContent = sequences[currentSequenceCounter];
				currentContent.show();
				checkNextPrevStatus();
				
			}
			
			
			
		}
		
		override public function goNext():void {
			if (currentContent)currentContent.hide();
			currentSequenceCounter++;
			if (currentSequenceCounter<sequences.length) {
				currentContent = sequences[currentSequenceCounter];
				currentContent.show();
				
			}
			
			checkNextPrevStatus();
			
			
		}

	}
}